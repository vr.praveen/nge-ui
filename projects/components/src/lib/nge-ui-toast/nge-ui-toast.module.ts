import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgeUiToastComponent } from './nge-ui-toast.component';



@NgModule({
  declarations: [NgeUiToastComponent],
  imports: [
    CommonModule
  ],
  exports: [NgeUiToastComponent]
})
export class NgeUiToastModule { }
