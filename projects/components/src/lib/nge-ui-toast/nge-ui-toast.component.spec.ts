import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgeUiToastComponent } from './nge-ui-toast.component';

describe('NgeUiToastComponent', () => {
  let component: NgeUiToastComponent;
  let fixture: ComponentFixture<NgeUiToastComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgeUiToastComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgeUiToastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
