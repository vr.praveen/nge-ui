import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgeUiChipComponent } from './nge-ui-chip.component';



@NgModule({
  declarations: [NgeUiChipComponent],
  imports: [
    CommonModule
  ],
  exports:[NgeUiChipComponent]
})
export class NgeUiChipModule { }
