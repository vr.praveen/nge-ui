import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgeUiChipComponent } from './nge-ui-chip.component';

describe('NgeUiChipComponent', () => {
  let component: NgeUiChipComponent;
  let fixture: ComponentFixture<NgeUiChipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgeUiChipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgeUiChipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
