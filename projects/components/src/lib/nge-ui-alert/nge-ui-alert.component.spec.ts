import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgeUiAlertComponent } from './nge-ui-alert.component';

describe('NgeUiAlertComponent', () => {
  let component: NgeUiAlertComponent;
  let fixture: ComponentFixture<NgeUiAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgeUiAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgeUiAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
