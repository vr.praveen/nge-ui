import { Component, OnInit, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'nge-ui-alert',
  templateUrl: './nge-ui-alert.component.html',
  styleUrls: ['./nge-ui-alert.component.scss']
})
export class NgeUiAlertComponent implements OnInit {
  @Input() status: String = 'light';
  @Input() content: String;
  @Input() template: TemplateRef<any>;
  constructor() { }

  ngOnInit() {
  }

}
