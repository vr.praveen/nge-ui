import { NgeUiAlertComponent } from './nge-ui-alert.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [NgeUiAlertComponent],
  imports: [
    CommonModule
  ],
  exports: [NgeUiAlertComponent]
})
export class NgeUiAlertModule { }
