import { NgModule } from '@angular/core';
import { NgeUiSpinnerModule } from './nge-ui-spinner/nge-ui-spinner.module';
import { NgeUiChipModule } from './nge-ui-chip/nge-ui-chip.module';
import { NgeUiAlertModule } from './nge-ui-alert/nge-ui-alert.module';
import { NgeUiProgressModule } from './nge-ui-progress/nge-ui-progress.module';
import { NgeUiToastModule } from './nge-ui-toast/nge-ui-toast.module';



@NgModule({
  declarations: [],
  imports: [ NgeUiAlertModule, NgeUiChipModule, NgeUiProgressModule, NgeUiSpinnerModule, NgeUiToastModule],
  exports: [NgeUiAlertModule, NgeUiChipModule, NgeUiProgressModule, NgeUiSpinnerModule, NgeUiToastModule]
})
export class NgeUiModule { }
