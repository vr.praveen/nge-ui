import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgeUiSpinnerComponent } from './nge-ui-spinner.component';

describe('NgeUiSpinnerComponent', () => {
  let component: NgeUiSpinnerComponent;
  let fixture: ComponentFixture<NgeUiSpinnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgeUiSpinnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgeUiSpinnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
