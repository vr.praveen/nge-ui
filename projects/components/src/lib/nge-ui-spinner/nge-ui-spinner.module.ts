import { NgeUiSpinnerComponent } from './nge-ui-spinner.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [NgeUiSpinnerComponent],
  imports: [
    CommonModule
  ],
  exports: [NgeUiSpinnerComponent]
})
export class NgeUiSpinnerModule { }
