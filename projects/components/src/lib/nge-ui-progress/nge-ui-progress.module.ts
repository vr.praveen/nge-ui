import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgeUiProgressComponent } from './nge-ui-progress.component';



@NgModule({
  declarations: [NgeUiProgressComponent],
  imports: [
    CommonModule
  ],
  exports: [NgeUiProgressComponent]
})
export class NgeUiProgressModule { }
