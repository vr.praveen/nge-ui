import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgeUiProgressComponent } from './nge-ui-progress.component';

describe('NgeUiProgressComponent', () => {
  let component: NgeUiProgressComponent;
  let fixture: ComponentFixture<NgeUiProgressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgeUiProgressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgeUiProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
