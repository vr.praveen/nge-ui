import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { NgeUiModule } from '@nge-ui/components';
import { NgeUiModule } from 'nge-ui-components';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // ComponentsModule,
    // NgeUiAlertModule,
    NgeUiModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
